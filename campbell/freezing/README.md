## Final Solidification Simulation

* Continues initial filling simulation, from 3 sec to 240 sec
* HT + phase change, no flow

```sh
mpiexec -np 10 truchas -r:restart-0p.bin freeze-0p.inp

```
