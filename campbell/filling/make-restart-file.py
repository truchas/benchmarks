#!/usr/bin/env python3

import sys
import numpy as np
import truchas

tdata = truchas.TruchasData(sys.argv[1])
sid = tdata.num_series()

vof = tdata.field(sid, "VOF")
vof[:,1] += vof[:,3]  # convert void to fluid
vof[:,3] = 0.0
tdata.assign_field(sid, "VOF", vof)

tdata.write_restart(sys.argv[2], sid)
