## Mold Filling Simulation

* First 3 sec
* Flow + HT with phase change

```sh
mpiexec -np 10 truchas fill-0p.inp

# generate restart file for final stage of the simulation
./make-restart-file.py fill-0p_output/fill-0p.h5 restart-0p.bin

```
