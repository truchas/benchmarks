# Isothermal Mold Filling

Isothermal flow simulation of filling the mold with liquid Al. No heat
transfer. Domain consists of the casting volume only -- no mold. This is
intended as a reference for the full non-isothermal flow simulations. 

Results using Truchas 24.04 (NAG 7.2, Fedora 39, AMD Threadripper 2920X)
