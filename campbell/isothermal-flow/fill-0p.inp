
&MESH
  mesh_file = "meshes/mesh0p-cast.gen"
  coordinate_scale_factor = 0.001
/

&OUTPUTS
  output_t  = 0.0, 2.0
  output_dt = 0.05
/

&PHYSICS
  flow = T
  body_force_density = 0.0, -9.81, 0.0
  materials = "aluminum", "VOID"
/

&PHYSICAL_CONSTANTS
  absolute_zero = -273.15
/

&NUMERICS
  dt_init = 1e-03
  dt_grow = 1.05
  dt_max = 10.0
  dt_min = 1e-06
/

### FLOW #######################################################################

&FLOW
  courant_number= 0.2
  viscous_implicitness = 0.5
  material_priority = "SOLID", "liquid", "VOID"
  vol_track_subcycles = 1
  void_collapse = T
  void_collapse_relaxation = 0.1
/

x&TURBULENCE
  length = 5e-3
/

&FLOW_PRESSURE_SOLVER
  abs_tol = 1e-10
  rel_tol = 0.0
  max_ds_iter = 500
  max_amg_iter = 20
/

&FLOW_VISCOUS_SOLVER
  abs_tol = 0.0
  rel_tol = 1e-08
  max_ds_iter = 100
  max_amg_iter = 10
/

&FLOW_BC
  name = "inlet"
  face_set_ids = 1
  type = "pressure"
  pressure = 1000.0
  inflow_material = "liquid"
  inflow_temperature = 700.0
/

&FLOW_BC
  name = "walls"
  face_set_ids = 3
  type = "no-slip"
/

### BODIES #####################################################################

Start with a bit of liquid at the top inlet of the sprue
&BODY
  surface_name = "box"
  translation_pt = 9.6e-3 4.10e-1, -7.5e-3
  length = 20e-3, 10e-3, 16e-3
  material_name = "liquid"
  temperature = 700
/

Rest of the mold cavity is void initially
&BODY
  surface_name = "from mesh file"
  mesh_material_number = 1, 3
  material_name = "VOID"
  temperature = 0.0
  velocity = 0.0, 0.0, 0.0
/

### MATERIALS ##################################################################

&MATERIAL
  name = "aluminum"
  phases = "solid", "liquid"
  density = 2400.0
/

&PHASE
  name = "solid"
  specific_heat = 1100.0
  conductivity  = 220.0
/

&PHASE
  name = "liquid"
  is_fluid = T
  specific_heat = 1100.0
  conductivity  = 110.0
  viscosity = 0.02
/

&PHASE_CHANGE
  low_temp_phase = "solid"
  high_temp_phase = "liquid"
  solid_frac_table =
      635.0, 1.0,
      640.0, 0.8669,
      645.0, 0.6327,
      650.0, 0.4426,
      659.0, 0.0848,
      660.4, 0.0
  latent_heat = 289292.0
/

&MATERIAL
  name = "sand"
  density = 1520.0
  specific_heat = 987.0
  conductivity  = 0.65
/
