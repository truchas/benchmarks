#!/bin/bash

# This assumes png files [0-2]p.[0000-0040].png are present

# combine the frames from the different meshes into one png
for x in {0000..0040}; do
montage [0-2]p.$x.png -mode concatenate -border 1 all.$x.png
done

# movie intro is 3 seconds freeze of frame 0
ffmpeg -loop 1 -framerate 5 -i all.0000.png -t 3 intro.mp4

# main movie
ffmpeg -framerate 5 -i all.%04d.png main.mp4
 
# movie outro is 1 second freeze of last frame
ffmpeg -loop 1 -framerate 5 -i all.0040.png -t 1 outro.mp4

cat << EOF > list.txt
file intro.mp4
file main.mp4
file outro.mp4
EOF

# concatenate the 3 bits into the final movie
ffmpeg -f concat -safe 0 -i list.txt -c copy fill-frac-all.mp4

# remove intermediate files
rm -rf list.txt all.*.png intro.mp4 outro.mp4 main.mp4
