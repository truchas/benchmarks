#!/bin/bash

# This script takes 1 argument: 0p, 1p, 2p
# This assumes png files $1.[0000-0040].png are present

# movie intro is 3 seconds freeze of frame 0
ffmpeg -loop 1 -framerate 5 -i $1.0000.png -t 3 intro.mp4

# main movie
ffmpeg -framerate 5 -i $1.%04d.png main.mp4
 
# movie outro is 1 second freeze of last frame
ffmpeg -loop 1 -framerate 5 -i $1.0040.png -t 1 outro.mp4

cat << EOF > list.txt
file intro.mp4
file main.mp4
file outro.mp4
EOF

# concatenate the 3 bits into the final movie
ffmpeg -f concat -safe 0 -i list.txt -c copy fill-frac-$1.mp4

# remove intermediate files
rm -rf list.txt intro.mp4 outro.mp4 main.mp4
