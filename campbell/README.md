# Campbell Benchmark Al Plate Casting

This directory contains Truchas models and numerical simulations of the filling
and solidification of an aluminum plate sand mold casting. The problem is a
benchmark test devised by Campbell [1] for 1995's 7th Conference on Modeling of
Casting, Welding and Advanced Solidification Processes.

The description of the benchmark test and other references can be found in the
`references` directory.

[1] Sirrell, B., Holliday, M., & Campbell, J. (1996). Benchmark testing the
flow and solidification modeling of AI castings. JOM, 48(3), 20-23.