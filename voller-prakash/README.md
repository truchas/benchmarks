# Voller-Prakash Mushy Zone Flow Test Problem

This directory contains Truchas models and numerical simulations of a test
problem used by Voller and Prakash [1]. The problem considers freezing in
a thermal cavity with natural convection and a porous media treatment of
flow in the two-phase mushy region.

[1] Voller, V. R., & Prakash, C. (1987). A fixed grid numerical modelling
methodology for convection-diffusion mushy region phase-change problems
International journal of heat and mass transfer, 30(8), 1709-1719.