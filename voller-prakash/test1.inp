VOLLER-PRAKASH TEST PROBLEM (1987)
----------------------------------

Solidification in a unit square domain with natural convection and a porous
media treatment of flow in the mushy zone.

This input includes the porous drag force with a coefficient of 1.6e3 used
in the Voller-Prakash paper. It also uses the trapezoid rule to discretize
the viscous and drag forces.

&MESH
  x_axis%coarse_grid = 0.0, 1.0
  x_axis%intervals   = 40
  y_axis%coarse_grid = 0.0, 1.0
  y_axis%intervals   = 40
  z_axis%coarse_grid = 0.0, 0.025
  z_axis%intervals   = 1
/

&OUTPUTS
  output_t = 0.0, 1000.0
  output_dt = 100.0
/

&PHYSICS
  heat_transport = T
  flow = T
  materials = 'stuff'
  body_force_density = 0.0, -1000.0, 0.0
/

&NUMERICS
  dt_init = 1e-3
  dt_grow = 1.2
  dt_max = 10.0
  dt_min = 1e-06
/

&DIFFUSION_SOLVER
  abs_temp_tol = 0.0
  rel_temp_tol = 1e-2
  abs_enthalpy_tol = 0.0
  rel_enthalpy_tol = 1e-2
  !max_nlk_itr = 8
  !nlk_tol = 0.1
  !pc_amg_cycles = 2
  !hypre_amg_strong_threshold = 0.25 ! recommended for 2D problems
/

&THERMAL_BC
  name = 'cold side'
  face_set_ids = 1
  type = 'temperature'
  temp = -0.5
/

&THERMAL_BC
  name = 'hot side'
  face_set_ids = 2
  type = 'temperature'
  temp = 0.5
/

&THERMAL_BC
  name = 'adiabatic'
  face_set_ids = 3, 4, 5, 6
  type = 'flux'
  flux = 0
/

&FLOW
  courant_number= 0.25
  vol_track_subcycles = 1
  viscous_implicitness = 0.5
  porous_drag = T
  drag_coef = 1.6e3
  drag_implicitness = 0.5
/

&FLOW_PRESSURE_SOLVER
  abs_tol = 1e-10
  rel_tol = 0.0
  max_ds_iter = 500
  max_amg_iter = 20
/

&FLOW_VISCOUS_SOLVER
  abs_tol = 0.0
  rel_tol = 1e-8
  max_ds_iter = 100
  max_amg_iter = 10
/

&FLOW_BC
  name = 'walls'
  face_set_ids = 1, 2, 3, 4
  type = 'no-slip'
/

&BODY
  surface_name = 'background'
  material_name = 'liquid'
  velocity = 0.0, 0.0, 0.0
  temperature = 0.5
/

&MATERIAL
  name = 'stuff'
  phases = 'solid', 'liquid'
  density = 1.0
  conductivity = 0.001
  specific_heat = 1.0
/

&PHASE
  name = 'liquid'
  is_fluid = T
  viscosity = 1.0
  density_delta_func = 'liquid-drho'
/

&FUNCTION
  name = 'liquid-drho'
  type = 'polynomial'
  poly_coefficients = -0.01
  poly_exponents = 1
  poly_refvars = 0.5
/

Voller used a linear solid fraction function. Truchas uses a smooth Hermite
cubic function whose maximum slope is 50% greater for the same solidification
interval. To match the maximum slope we use an interval that is 50% larger.

&PHASE_CHANGE
  low_temp_phase  = 'solid'
  high_temp_phase = 'liquid'
  solidus_temp    = -0.15 ! voller -0.1
  liquidus_temp   =  0.15 ! voller  0.1
  latent_heat     = 5.0
/
